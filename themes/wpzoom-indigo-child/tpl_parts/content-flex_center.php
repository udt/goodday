<?php
/**
 * The template used for displaying page content with flex-centered content
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="flex flex_center js_full-height">
        <div class="flex-content text-center">
            <?php the_content(); ?>
        </div>
    </div><!-- .entry-content -->

</article><!-- #post-## -->