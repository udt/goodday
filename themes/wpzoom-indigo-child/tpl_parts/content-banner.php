<?php
/**
 * The template used for displaying page content with banner topimage
 */

$banner_top_image = carbon_get_the_post_meta('banner_top_image');

$left_col_content = carbon_get_the_post_meta('left_column_content');
$left_col_html = carbon_get_the_post_meta('contact_form_init');

$right_col_content = carbon_get_the_post_meta('right_column_content');
$right_col_bg_image = carbon_get_the_post_meta('right_column_background_image');
$right_col_bg_color = carbon_get_the_post_meta('right_column_background_color');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="banner banner-top text-center" style="background: url('<?php echo $banner_top_image; ?>') no-repeat center center; background-size: cover;">
		<div class="h1"><?php the_title(); ?></div>
	</div>

	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="flex end-md text-left">
				<div class="twocol-container">
					<?php echo $left_col_content; ?>
					<?php echo do_shortcode($left_col_html); ?>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12" style="background: url('<?php echo $right_col_bg_image; ?>') no-repeat center center; background-size: cover; background-color: <?php echo $right_col_bg_color; ?>; ">
			<div class="flex start-md">
				<div class="twocol-container twocol-container-right">
					<?php echo $right_col_content; ?>
				</div>
			</div>
		</div>
	</div>
	<?php the_content(); ?>

</div><!-- #post-## -->