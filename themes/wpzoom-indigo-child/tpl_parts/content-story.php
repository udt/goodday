<?php
/**
 * The template used for displaying page story content
 */
$slider_shortcode = carbon_get_the_post_meta('slider_slogan_shortcode');
$niki_image = carbon_get_the_post_meta('niki_image');
$page_subtitle = carbon_get_the_post_meta('page_subtitle');
$page_quote = carbon_get_the_post_meta('page_quote');

$subscribe_bg = carbon_get_the_post_meta('subscribe_bg');
$subscribe_text = carbon_get_the_post_meta('subscribe_text');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php if($slider_shortcode){ ?>  
		<div class="banner banner-slide">
			<?php echo do_shortcode($slider_shortcode); ?>
		</div>
	<?php } ?>
	<div class="page-content" style="background: url('<?php echo $niki_image; ?>') no-repeat bottom right; background-size: contain;">
		<div class="inner-wrap">
			<div class="page-subtitle">
				<?php echo $page_subtitle; ?>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<?php the_content(); ?>
					<div class="page-quote">
						<?php echo $page_quote; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-top subscribe" style="background: url('<?php echo $subscribe_bg; ?>') no-repeat center center; background-size: cover;">
		<div class="inner-wrap">
			<div class="row">
				<div class="col-sm-7">
					<div class="h2"><?php echo $subscribe_text; ?></div>
				</div>
				<div class="col-sm-5">
					
				</div>
			</div>
		</div>
	</div>
</div><!-- #post-## -->