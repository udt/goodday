<?php
/**
* The template for displaying the footer
*
*/

?>

</div><!-- ./inner-wrap -->

<footer id="colophon" class="site-footer b_footer" role="contentinfo">
  <div class="footer-widgets widgets b_footer-widgets">
    <div class="inner-wrap">
      <div class="widget-areas">
        <?php if ( is_active_sidebar( 'footer_1' ) ) : ?>
          <div class="column b_footer-col">
            <?php dynamic_sidebar('footer_1'); ?>
            <div class="clear"></div>
          </div><!-- end .column -->
        <?php endif; ?>

        <?php if ( is_active_sidebar( 'footer_2' ) ) : ?>
          <div class="column b_footer-col">
            <?php dynamic_sidebar('footer_2'); ?>
            <div class="clear"></div>
          </div><!-- end .column -->
        <?php endif; ?>

        <?php if ( is_active_sidebar( 'footer_3' ) ) : ?>
          <div class="column b_footer-col">
            <?php dynamic_sidebar('footer_3'); ?>
            <div class="clear"></div>
          </div><!-- end .column -->
        <?php endif; ?>

        <?php if ( is_active_sidebar( 'footer_4' ) ) : ?>
          <div class="column b_footer-col">
            <?php dynamic_sidebar('footer_4'); ?>
            <div class="clear"></div>
          </div><!-- end .column -->
        <?php endif; ?>
      </div><!-- .widget-areas -->
    </div><!-- .inner-wrap -->
  </div><!-- .footer-widgets -->


  <?php if ( is_active_sidebar( 'widgetized_section' ) ) : ?>
    <section class="site-widgetized-section section-footer">
      <div class="widgets clearfix">
        <?php dynamic_sidebar( 'widgetized_section' ); ?>
      </div>
    </section><!-- .site-widgetized-section -->
  <?php endif; ?>


</footer><!-- #colophon -->

</div><!-- /.page-wrap -->

<?php wp_footer(); ?>

</body>
</html>