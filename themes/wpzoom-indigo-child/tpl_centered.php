<?php
/*
Template Name: Coming soon
Template Post Type: page
*/

get_header(); ?>

    <main id="main" class="site-main" role="main">

    	<div class="inner-content">

        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'tpl_parts/content', 'flex_center' ); ?>

        <?php endwhile; // end of the loop. ?>

       </div>

    </main><!-- #main -->

<?php get_footer();