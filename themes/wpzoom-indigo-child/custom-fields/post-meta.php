<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Page settings')
  ->show_on_page('contact-us')
  ->add_fields(array(
  	Field::make('image', 'banner_top_image')->set_value_type('url')
  ))
  ->add_tab('Column left', array(
  	Field::make("rich_text", "left_column_content", "Input left column content here"),
  	Field::make("text", "contact_form_init", "Contact form shortcode")
  ))
  ->add_tab('Column Right', array(
  	Field::make("color", "right_column_background_color", "Background Color"),
  	Field::make('image', 'right_column_background_image', "Background Image")->set_value_type('url'),
  	Field::make('rich_text', 'right_column_content', 'Input right column content here')
  ));

Container::make('post_meta', 'Page settings')
  ->show_on_page('our-story')
  ->add_fields(array(
    Field::make("text", "slider_slogan_shortcode", "Slider shortcode"),
    Field::make('image', 'niki_image', "Background Image")->set_value_type('url'),
    Field::make("text", "page_subtitle", "Page subtitle"),
    Field::make("text", "page_quote", "Page quote")
  ));

Container::make('post_meta', 'Subscribe settings')
  ->show_on_page('our-story')
  ->add_fields(array(
    Field::make("text", "subscribe_text", "Description"),
    Field::make('image', 'subscribe_bg', "Background Image")->set_value_type('url')
  ));

?>