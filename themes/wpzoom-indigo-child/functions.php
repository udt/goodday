<?

define("WIDGETS_INC", get_stylesheet_directory() . "/widgets");

require_once WIDGETS_INC . "/mostviewed.php";

/* Enqueue scripts and styles for the front end.
=========================================== */

function indigo_child_scripts(){
	// styles
	wp_enqueue_style( 'indigo-style', get_stylesheet_directory_uri() . '/style.css', array(), true );
    wp_enqueue_style( 'goodday_css_libs', get_stylesheet_directory_uri() . '/css/libs/libs.min.css', array(), true );
	wp_enqueue_style( 'goodday_style', get_stylesheet_directory_uri() . '/css/main.min.css?v=' . filemtime(get_stylesheet_directory() . '/css/main.min.css'), array(), true );
	//
	// scripts
	wp_enqueue_script( 'goodday_script', get_stylesheet_directory_uri() . '/js/main.min.js' );
}
add_action( 'wp_enqueue_scripts', 'indigo_child_scripts' );


/* requuire custom fields 
============================================*/
function crb_register_custom_fields(){
	require_once __DIR__ . '/custom-fields/post-meta.php';
}
add_action('carbon_register_fields', 'crb_register_custom_fields');


add_filter('excerpt_more', function($more) {
    return '...';
});

/* check if plugin is already installed 
============================================*/
if ( ! function_exists( 'carbon_get_post_meta' ) ) {
    function carbon_get_post_meta( $id, $name, $type = null ) {
        return false;
    }   
}

if ( ! function_exists( 'carbon_get_the_post_meta' ) ) {
    function carbon_get_the_post_meta( $name, $type = null ) {
        return false;
    }   
}

if ( ! function_exists( 'carbon_get_theme_option' ) ) {
    function carbon_get_theme_option( $name, $type = null ) {
        return false;
    }   
}

if ( ! function_exists( 'carbon_get_term_meta' ) ) {
    function carbon_get_term_meta( $id, $name, $type = null ) {
        return false;
    }   
}

if ( ! function_exists( 'carbon_get_user_meta' ) ) {
    function carbon_get_user_meta( $id, $name, $type = null ) {
        return false;
    }   
}

if ( ! function_exists( 'carbon_get_comment_meta' ) ) {
    function carbon_get_comment_meta( $id, $name, $type = null ) {
        return false;
    }   
}

/* remove empty p
============================================*/
add_filter('the_content', 'remove_empty_p', 20, 1);
function remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}