<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="page-wrap">

  <header class="site-header b_header">
    <!-- top video -->
    <div class="b_header_video" style="background: url('<?php echo option::get('banner_top'); ?>') no-repeat center center; background-size: cover;">
      <?php if (option::is_on('ad_head_select')) { ?>

          <?php if ( option::get('ad_head_code') <> "") {
            echo stripslashes(option::get('ad_head_code'));
          } else { ?>
            <!--<a href="<?php //echo option::get('banner_top_url'); ?>">
              <img src="<?php //echo option::get('banner_top'); ?>" alt="<?php //echo option::get('banner_top_alt'); ?>" />
            </a>-->
          <?php } ?>

        <div class="clear"></div>
      <?php } ?>
    </div>
    <!-- .top video -->
    <nav class="b_header_navbar top-navbar" role="navigation">

        <div class="inner-wrap b_header_flex">
          
          <div class="b_header-left">

            <div class="navbar-header-main b_header_navbarmobile">
              <?php if ( has_nav_menu( 'mobile' ) ) { ?>

               <?php wp_nav_menu( array(
                 'container_id'   => 'menu-main-slide',
                 'theme_location' => 'mobile',
                 'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s' . indigo_wc_menu_cartitem() . '</ul>'
               ) );
             } elseif ( has_nav_menu( 'primary' ) ) {
                wp_nav_menu( array(
                   'container_id'   => 'menu-main-slide',
                  'theme_location' => 'primary',
                  'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s' . indigo_wc_menu_cartitem() . '</ul>'
               ) );
              } ?>
            </div>

            <div class="b_header_brand">
              <?php indigo_custom_logo() ?>
            </div><!-- .navbar-brand -->

            <div id="navbar-top" class="b_header_navbartop">
              <?php if (has_nav_menu( 'secondary' )) {
                  wp_nav_menu( array(
                      'menu_class'     => 'navbar-wpz dropdown sf-menu',
                      'theme_location' => 'secondary'
                  ) );
              } ?>
            </div><!-- #navbar-top -->
          </div>

          <div class="b_header-center b_header-mobile">
            <div class="b_header_brand">
              <?php indigo_custom_logo() ?>
            </div><!-- .navbar-brand -->
          </div>

          <div class="b_header-right">
            <div class="header_social">
                <?php dynamic_sidebar('header_social'); ?>
            </div><!-- .header_social -->
            <div id="sb-search" class="sb-search">
                <?php get_search_form(); ?>
            </div><!-- .sb-search -->            
          </div>

        </div><!-- ./inner-wrap -->

    </nav><!-- .navbar -->

  </header><!-- .site-header -->

  <?php if( is_page_template( array('tpl_banner.php', 'tpl_story.php' )) || is_front_page() ){
      echo '<div class="inner-wrap full-width">';
    } else {
      echo '<div class="inner-wrap">';
    }
  ?>