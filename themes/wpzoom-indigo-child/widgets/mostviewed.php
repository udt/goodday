<?php

/*------------------------------------------*/
/* WPZOOM: Popular Tabs Widget              */
/*------------------------------------------*/

class Wpzoom_Popular_Views_Child extends WP_Widget {

	function __construct() {

		/* Widget settings. */
		$widget_ops_child = array( 'classname' => 'popular-views_child', 'description' => 'A list of the most viewed posts' );

		/* Widget control settings. */
		$control_ops_child = array( 'id_base' => 'wpzoom-popular-views_child' );

		/* Create the widget. */
		parent::__construct( 'wpzoom-popular-views_child', 'WPZOOM CHILD: Most Viewed', $widget_ops_child, $control_ops_child );

	}

	function widget( $args, $instance ) {

		extract( $args );

		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$show_count = $instance['show_count'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		?>
			<?php $loop = new WP_Query( array( 'ignore_sticky_posts' => true, 'showposts' => $show_count,  'meta_key' => 'Views', 'orderby' => 'meta_value_num', 'order' => 'DESC' ) ); ?>

			<!-- good day popular posts -->
			<ul class="gd_pp row">
				<?php if ( $loop->have_posts() ) : ?>
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<li class="gd_pp_col col-md-3 col-sm-4">
							<div class="gd_pp_wrapper">
								<div class="gd_pp_image">
									<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
								</div>
								<div class="gd_pp_content">
									<?php printf( '<span class="cat-links">%s</span>', get_the_category_list( ' ' )); ?>
									<h3 class="gd_pp_title">
										<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
											<?php the_title(); ?>
										</a>
									</h3>
									<div class="gd_pp_content-in">
										<p>
											<?php the_excerpt(); ?>
										</p>
									</div>
									<span class="post-meta"><?php echo get_the_date('M j, Y');  ?> </span>
									<div class="gd_pp_button readmore_button">
										<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">read more</a>
									</div>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>

			<?php endif; ?>
		 <?php

		/* After widget (defined by themes). */
		echo $after_widget;

	}

	function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_count'] = $new_instance['show_count'];

		return $instance;

	}

	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Most Viewed', 'show_count' => 5 );
		$instance = wp_parse_args( (array) $instance, $defaults );


		?>

			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'wpzoom'); ?></label><br />
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" type="text"  class="widefat" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'show_count' ); ?>">Show:</label>
				<input id="<?php echo $this->get_field_id( 'show_count' ); ?>" name="<?php echo $this->get_field_name( 'show_count' ); ?>" value="<?php echo $instance['show_count']; ?>" type="text" size="2" /> posts
			</p>

		<?php

	}

}

add_action('widgets_init', 'register_widget_child');
function register_widget_child(){
	register_widget('Wpzoom_Popular_Views_Child');
}