<?php
/*
Template Name: Banner Top
Template Post Type: page
*/

get_header(); ?>

    <main id="main" class="site-main full-width-tpl" role="main">

      <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'tpl_parts/content', 'banner' ); ?>

      <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->

<?php get_footer();