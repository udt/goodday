jQuery(document).ready(function($){
	/*---------------------
		Full Height Content
	----------------------*/

	function full_height_init(){
		var _windowHeight = $(window).height();
		var _footerHeight = $(".b_footer").outerHeight();
		var _headerHeight = $(".b_header").outerHeight();
		var _contentHeight = _windowHeight - _footerHeight - _headerHeight;
		if ($(window).width() >= 768){
			$(".js_full-height").height(function(){
				return _contentHeight;
			});
		}
	}

	/*---------------------
			REMOVE EMPTY P
	----------------------*/

	$('p:empty').remove();

	/*---------------------
						Init
	----------------------*/
	full_height_init();

	$(window).resize(function(){
		var _windowHeight = $(window).height();
		var _footerHeight = $(".b_footer").outerHeight();
		var _contentHeight = _windowHeight - _footerHeight;
		full_height_init();
	})

});