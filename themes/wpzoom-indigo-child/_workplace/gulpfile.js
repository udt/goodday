//path
let pathRoot     = 'http://goodday.loc/',
    pathDev      = './dev',
    pathDist     = './dist',
    pathWp       = './../',
    pathAssets   = './dev/assets',
    pathCssLibs  = pathAssets + '/css/libs',
    pathSass     = pathAssets + '/sass',
    pathCss      = pathAssets + '/css',
    pathJs       = pathAssets + '/js',
    pathImg      = pathAssets + '/img',
    pathLibs     = pathAssets + '/libs',
    pathFonts    = pathAssets + '/fonts';

//gulp file
let gulp         = require( 'gulp' ),
    sass         = require( 'gulp-sass' ),
    browserSync  = require( 'browser-sync' ),
    pug          = require( 'gulp-pug' ),
    concat       = require( 'gulp-concat' ),
    uglify       = require( 'gulp-uglifyjs' ),
    cssnano      = require( 'gulp-cssnano' ),
    rename       = require( 'gulp-rename' ),
    del          = require( 'del' ),
    imagemin     = require( 'gulp-imagemin'),
    pngquant     = require( 'imagemin-pngquant' ),
    cache        = require( 'gulp-cache' ),
    autoprefixer = require( 'gulp-autoprefixer' ),
    concatCss    = require( 'gulp-concat-css' );

gulp.task('pug', () => {
    return gulp.src( pathDev+'/*.pug' )
        .pipe( pug({
            pretty: true
        }) )
        .pipe( gulp.dest( pathDev ) )
        .pipe( browserSync.reload( {stream: true} ) );
});

gulp.task('sass', () => {
    return gulp.src( pathSass+'/*.+(sass|scss)' )
        .pipe( sass() )
        .pipe( autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {'cascade': true}) )
        .pipe( rename('main.css') )
        .pipe( gulp.dest( pathCss ) )
        .pipe( rename({suffix: '.min'}) )
        .pipe( cssnano() )
        .pipe( gulp.dest( pathCss) )
        .pipe( gulp.dest( pathWp+'/css' ) )
        .pipe( browserSync.reload( {stream: true} ) )
});

gulp.task('cssLibs', () => {
    return gulp.src( pathCssLibs+'/*.css' )
        .pipe(concat('libs.min.css'))
        .pipe( gulp.dest( pathWp+'/css/libs' ) )
        .pipe( browserSync.reload( {stream: true} ) )
});

gulp.task('scripts', () => {
    return gulp.src([
        pathLibs+'/moment-2.7.0/moment.min.js',
        pathLibs+'/jquery-3.2.1/jquery.countdown.min.js',
        pathLibs+'/jquery-3.2.1/jquery.plugin.min.js',
        pathLibs+'/aos/aos.min.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest( pathWp+'/js' ))
        .pipe(gulp.dest( pathJs ));
});

gulp.task('del-main-js', () => {
    return del.sync(pathJs+'/main.min.js');
});
gulp.task('js', ['del-main-js'], () => {
    return gulp.src( pathJs + '/*.js')
        .pipe( concat('main.js') )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe(gulp.dest( pathWp+'/js' ))
        .pipe( gulp.dest(pathJs) );
});

gulp.task('fonts', function(){
    return gulp.src( pathFonts+'/*.+(woff|ttf|woff2)')
        .pipe( gulp.dest(pathWp+'/fonts' ) )
        //.pipe( gulp.dest(pathDist + '/assets/fonts') );
});
// gulp.task('css-libs', () => {
//     return gulp.src([
//         pathLibs+'/bootstrap.css'
//     ])
//         .pipe( concatCss( 'libs.css' ) )
//         .pipe( gulp.dest( pathCss ))
// });
//
// gulp.task('min-css-libs', ['sass', 'css-libs'], () => {
//     return gulp.src( pathCss+'/libs.css' )
//         .pipe( cssnano() )
//         .pipe( rename({ suffix: '.min' }) )
//         .pipe(gulp.dest( pathCss ));
// });

gulp.task('browser-sync', () => {
    browserSync({
        proxy: pathRoot,
        notify: false
    });
});

gulp.task('clean', () => {
   return del.sync(pathDist);
});

gulp.task('clear', () => {
    return cache.clearAll();
});

gulp.task('img', function() {
   return gulp.src(pathImg+'/**/**/*')
       .pipe(cache(imagemin({
           interlaced: true,
           progressive: true,
           svgoPlugins: [{removeViewBox: false}],
           use: [pngquant()]
       })))
       .pipe( gulp.dest(pathWp+'/img' ) )
       //.pipe(gulp.dest( pathDist+'/assets/img' ));
});

gulp.task('watch', ['pug', 'js', 'sass', 'img', 'browser-sync'], () => {
    gulp.watch( pathSass+'/**/*.+(sass|scss)', ['sass'] );
    gulp.watch( pathDev+'/*.pug', ['pug']);
    gulp.watch( pathJs+'/*.js', ['js']);
    gulp.watch( pathImg+'/**/**/*', ['img']);
});

gulp.task('build', ['clean', 'img', 'sass', 'js'], () => {
    let buildCss = gulp.src([
        pathCss+'/main.min.css'
    ])
        //.pipe(gulp.dest(pathDist+'/assets/css'));

    //let buildFonts = gulp.src(pathAssets+'/fonts/**/*')
        //.pipe(gulp.dest(pathDist+'/assets/fonts'));

    //let buildJs = gulp.src(pathJs+'/main.min.js')
        //.pipe(gulp.dest(pathDist+'/assets/js'));

    //let buildHtml = gulp.src(pathDev+'/*.html')
        //.pipe(gulp.dest(pathDist));
});