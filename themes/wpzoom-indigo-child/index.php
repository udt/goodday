<?php
/**
 * The main template file.
 */

    get_header();

    $template = option::get( 'layout_home' );

?>


<?php if ( option::is_on( 'featured_posts_show' ) && is_front_page() && $paged < 2) : // Featured Slider ?>
    <?php get_template_part( 'wpzoom-slider' ); ?>
<?php endif; ?>

<?php if ( is_active_sidebar( 'homepage-top' ) && is_front_page() && $paged < 2  ) : ?>
    <section class="home-widgetized-sections">
        <div class="inner-wrap">
            <?php dynamic_sidebar( 'homepage-top' ); ?>
        </div>
    </section><!-- .home-widgetized-sections -->
<?php endif; ?>

<?php if ( is_front_page() && $paged < 2) : ?>

    <?php if ( is_active_sidebar( 'homepage-1' ) ||  is_active_sidebar( 'homepage-2'  )  ||  is_active_sidebar( 'homepage-3'  ) ) : ?>
        <div class="column-widgets">

            <?php if ( is_active_sidebar( 'homepage-1'  ) ) { ?>
                <div class="widget-column">
                    <?php dynamic_sidebar('homepage-1'); ?>
                </div><!-- .column -->
            <?php } ?>

            <?php if ( is_active_sidebar( 'homepage-2'  ) ) { ?>
                <div class="widget-column">
                    <?php dynamic_sidebar('homepage-2'); ?>
                </div><!-- .column -->
            <?php } ?>

            <?php if ( is_active_sidebar( 'homepage-3'  ) ) { ?>
                <div class="widget-column">
                    <?php dynamic_sidebar('homepage-3'); ?>
                </div><!-- .column -->
            <?php } ?>

        </div>

    <?php endif; ?>

<?php endif; ?>

<main id="main" class="site-main" role="main">

    <div class="inner-wrap">

        <section class="content-area<?php if ( 'full' == $template || option::get('post_view') == '3 Columns' ) { echo 'home_news_layout'; } ?>">

            <?php if (!option::is_on('recentposts_hide') ) { // Show Recent Posts? ?>

                <h2 class="section-title<?php if ( ( $template  == 'full' ) && (option::get('post_view') == 'Blog') ) { echo ' home_news_title title-bordered'; } ?>">

                    <?php if ( is_front_page() ) : ?><?php echo esc_html( option::get('recent_title') ); ?>

                    <?php else: ?>

                        <?php echo get_the_title( get_option( 'page_for_posts' ) ); ?>

                    <?php endif; ?>

                 </h2>

                <?php if ( have_posts() ) : ?>

                    <section id="recent-posts" class="latest-posts<?php if (option::get('post_view') == 'Blog') { echo " blog-inline"; } elseif (option::get('post_view') == '2 Columns') { echo " two-columns_layout"; } elseif ( option::get('post_view') == '3 Columns') { echo " three-columns_layout"; } ?>">

                        <?php $banner = 0; while ( have_posts() ) : the_post(); $banner++; ?>

                            <?php get_template_part( 'content', get_post_format() ); ?>

                            <?php if ( $banner == 1 && !is_paged() ) {

                                    if ( option::is_on('ad_slider_select')  ) { // Banner after first post ?>

                                        <div class="adv_content">
                                        <?php
                                            if ( option::get('ad_slider_code') <> "" ) {
                                                echo stripslashes(option::get('ad_slider_code'));
                                            } else {
                                                ?><a href="<?php echo option::get('banner_slider_home_url'); ?>" class="btn btn-def"><img src="<?php echo option::get('banner_slider_home'); ?>" alt="<?php echo option::get('banner_slider_home_alt'); ?>" /></a><?php
                                            }

                                        ?></div><?php
                                    }
                            } ?>


                        <?php endwhile; ?>

                    </section>

                    <?php //get_template_part( 'pagination' ); ?>

                <?php else: ?>

                    <?php get_template_part( 'content', 'none' ); ?>

                <?php endif; ?>

            <?php } // Hide Recent Posts option? ?>


            <?php if ( is_home() && $paged < 2 ) { ?>

                <?php if ( is_active_sidebar( 'home-categories' ) ) { // Widget area for Featured Categories ?>

                    <?php dynamic_sidebar('home-categories'); ?>

                <?php } ?>

            <?php } ?>

            <div class="clear"></div>

        </section><!-- .content-area -->

    </div>

    <?php if ( !( 'full' == $template  ||  option::get('post_view') == '3 Columns' ) ) : ?>

        <?php get_sidebar(); ?>

    <?php else : ?>

        <div class="clear"></div>

    <?php endif; ?>


</main><!-- .site-main -->

<?php
get_footer();
