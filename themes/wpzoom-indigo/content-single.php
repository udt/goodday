<?php
    $template = get_post_meta($post->ID, 'wpzoom_post_template', true);
    if ($template == 'full') {
        $media_width = 1200;
        $size = "loop-full";
    }
    else {
        $media_width = 800;
        if ( option::is_on( 'post_thumb_aspect' ) ) {
            $size = "loop-large";
        } else {
            $size = "loop-large-cropped";
        }
    }
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if ( option::is_on( 'post_thumb' ) && has_post_thumbnail() ) { ?>

        <header class="entry-image">
            <div class="post-thumb">
                <?php the_post_thumbnail($size, array( 'class' => 'photo' )); ?>
            </div>
        </header>
    <?php } ?>



    <div class="post_wrapper_main">

        <?php if ( function_exists( 'sharing_display' ) ) { ?>

            <div class="wpz-float-sharing"><?php echo sharing_display(); ?></div>

        <?php } ?>


        <div class="post_wrapper">

            <header class="entry-header">

                <?php if ( option::is_on( 'post_category' ) ) printf( '<span class="cat-links">%s</span>', get_the_category_list( ' ' ) ); ?>

                <?php the_title( '<h1 class="entry-title fn">', '</h1>' ); ?>
                <div class="entry-meta">
                    <?php if ( option::is_on( 'post_author' ) )   { printf( '<span class="entry-author">%s ', __( 'Written by', 'wpzoom' ) ); the_author_posts_link(); print('</span>'); } ?>
                    <?php if ( option::is_on( 'post_date' ) )     : ?><span class="entry-date"><?php _e( 'on', 'wpzoom' ); ?> <?php printf( '<time class="entry-date" datetime="%1$s">%2$s</time> ', esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) ); ?></span> <?php endif; ?>
                    <?php edit_post_link( __( 'Edit', 'wpzoom' ), '<span class="edit-link">', '</span>' ); ?>
                </div>
            </header><!-- .entry-header -->


            <div class="entry-content">
                <?php the_content(); ?>
                <div class="clear"></div>
                <?php if ( option::is_on('banner_post_enable')  ) { // Banner after post ?>
                    <div class="adv_content">
                    <?php
                        if ( option::get('banner_post_html') <> "" ) {
                            echo stripslashes(option::get('banner_post_html'));
                        } else {
                            ?><a href="<?php echo option::get('banner_post_url'); ?>"><img src="<?php echo option::get('banner_post'); ?>" alt="<?php echo option::get('banner_post_alt'); ?>" /></a><?php
                        }
                    ?></div><?php
                } ?>
            </div><!-- .entry-content -->

        </div>
    </div>

    <div class="clear"></div>

    <footer class="entry-footer">
        <?php
            wp_link_pages( array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'wpzoom' ),
                'after'  => '</div>',
            ) );
        ?>

        <?php if ( option::is_on( 'post_tags' ) ) : ?>
            <?php the_tags( '<div class="tag_list">', ' ',  '</div>'  ); ?>
        <?php endif; ?>


       <?php if ( option::is_on( 'post_author_box' ) ) : ?>

           <div class="post_author">

               <?php echo get_avatar( get_the_author_meta( 'ID' ) , 100 ); ?>

               <div class="author-description">

                   <h3 class="author-title author"><?php the_author_posts_link(); ?></h3>

                   <p class="author-bio">
                       <?php the_author_meta( 'description' ); ?>
                   </p>

                   <div class="author_links">

                       <?php if ( get_the_author_meta( 'facebook_url' ) ) { ?><a class="author_facebook" href="<?php the_author_meta( 'facebook_url' ); ?>" title="Facebook Profile" target="_blank">Facebook</a><?php } ?>

                       <?php if ( get_the_author_meta( 'twitter' ) ) { ?><a class="author_twitter" href="https://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on Twitter" target="_blank">Twitter</a><?php } ?>

                       <?php if ( get_the_author_meta( 'instagram_url' ) ) { ?><a class="author_instagram" href="https://instagram.com/<?php the_author_meta( 'instagram_url' ); ?>" title="Instagram" target="_blank">Instagram</a><?php } ?>

                   </div>

               </div>

               <div class="clear"></div>

           </div>

       <?php endif; ?>


        <?php if ( is_active_sidebar( 'sidebar-post' ) ) : ?>
            <section class="site-widgetized-section section-single">
                <?php dynamic_sidebar( 'sidebar-post' ); ?>
            </section><!-- .site-widgetized-section -->
        <?php endif; ?>


        <?php if ( option::is_on( 'post_prevnext' ) ) : ?>
            <div class="prevnext">
                <?php
                $previous_post = get_previous_post();
                $next_post = get_next_post();
                if ( $previous_post != NULL ) {
                    ?><div class="previous_post_pag">

                        <h4 class="prev_next_small"><?php _e('Previous', 'wpzoom'); ?></h4>

                        <a class="prevnext_title" href="<?php echo get_permalink($previous_post->ID); ?>" title="<?php echo $previous_post->post_title; ?>">
                            <div class="prevnext_container">

                            <h4><?php echo $previous_post->post_title; ?></h4>

                            </div>

                              <?php if ( has_post_thumbnail( $previous_post->ID ) ) {
                                // echo '<a href="' . get_permalink( $previous_post->ID ) . '" title="' . esc_attr( $previous_post->post_title ) . '">';
                                echo get_the_post_thumbnail( $previous_post->ID, 'loop' );
                                // echo '</a>';
                            } ?>

                        </a>
                    </div><?php
                }

                if ( $next_post != NULL ) {
                    ?><div class="next_post_pag">

                        <h4 class="prev_next_small"><?php _e('Next', 'wpzoom'); ?></h4>

                        <a class="prevnext_title" href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo $next_post->post_title; ?>">
                            <div class="prevnext_container">

                              <h4><?php echo $next_post->post_title; ?></h4>

                            </div>

                            <?php if ( has_post_thumbnail( $next_post->ID ) ) {
                                echo get_the_post_thumbnail( $next_post->ID, 'loop' );
                            } ?>
                        </a>
                    </div><?php
                }
                ?>
            </div>
        <?php endif; ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->