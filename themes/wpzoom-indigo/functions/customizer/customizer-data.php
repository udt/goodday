<?php


function indigo_customizer_data()
{
    static $data = array();

    if(empty($data)){

        $media_viewport = 'screen and (min-width: 768px)';

        $data = array(
            'color-palettes-container' => array(
                'title' => __('Color Scheme', 'wpzoom'),
                'priority' => 40,
                'options' => array(
                    'color-palettes' => array(
                        'setting' => array(
                            'default' => 'default',
                            'sanitize_callback' => 'sanitize_text_field'
                        ),
                        'control' => array(
                           'type' => 'select',
                           'label' => __('Color Scheme', 'wpzoom'),
                           'choices' => array(
                               'default' => __('Default', 'wpzoom'),
                               'green' => __('Green', 'wpzoom'),
                               'purple' => __('Purple', 'wpzoom'),
                               'blue' => __('Blue', 'wpzoom'),
                               'red' => __('Red', 'wpzoom'),
                               'brown' => __('Brown', 'wpzoom'),
                               'pink' => __('Pink', 'wpzoom')
                           )
                       ),
                        'dom' => array(
                            // * - mean that it is dynamic and would be from select choices.
                            'selector' => 'indigo-style-color-*-css',
                            'rule' => 'change-stylesheet'
                        )
                    ),
                )
            ),

            'title_tagline' => array(
                'title' => __('Site Identity', 'wpzoom'),
                'priority' => 20,
                'options' => array(
                    'hide-tagline' => array(
                        'setting' => array(
                            'sanitize_callback' => 'absint',
                            'default' => true
                        ),
                        'control' => array(
                            'label' => __('Show Tagline', 'wpzoom'),
                            'type' => 'checkbox',
                            'priority' => 11
                        ),
                        'style' => array(
                            'selector' => '.navbar-brand-wpz .tagline',
                            'rule' => 'display'
                        )
                    ),
                    'custom_logo_retina_ready' => array(
                        'setting' => array(
                            'sanitize_callback' => 'absint',
                            'default' => false,
                        ),
                        'control' => array(
                            'label' => __('Retina Ready?', 'wpzoom'),
                            'type' => 'checkbox',
                            'priority' => 9
                        ),
                        'partial' => array(
                            'selector' => '.navbar-brand-wpz a',
                            'container_inclusive' => false,
                            'render_callback' => 'indigo_custom_logo'
                        )
                    ),
                    'blogname' => array(
                        'setting' => array(
                            'sanitize_callback' => 'sanitize_text_field',
                            'default' => get_option('blogname'),
                            'transport' => 'postMessage',
                            'type' => 'option'
                        ),
                        'control' => array(
                            'label' => __('Site Title', 'wpzoom'),
                            'type' => 'text',
                            'priority' => 9
                        ),
                        'partial' => array(
                            'selector' => '.navbar-brand-wpz a',
                            'container_inclusive' => false,
                            'render_callback' => 'zoom_customizer_partial_blogname'
                        )
                    ),
                    'blogdescription' => array(
                        'setting' => array(
                            'sanitize_callback' => 'sanitize_text_field',
                            'default' => get_option('blogdescription'),
                            'transport' => 'postMessage',
                            'type' => 'option'
                        ),
                        'control' => array(
                            'label' => __('Tagline', 'wpzoom'),
                            'type' => 'text',
                            'priority' => 10
                        ),
                        'partial' => array(
                            'selector' => '.navbar-brand-wpz .tagline',
                            'container_inclusive' => false,
                            'render_callback' => 'zoom_customizer_partial_blogdescription'
                        )
                    ),
                    'custom_logo' => array(
                        'partial' => array(
                            'selector' => '.navbar-brand-wpz a',
                            'container_inclusive' => false,
                            'render_callback' => 'indigo_custom_logo'
                        )
                    )
                )
            ),
            'header' => array(
                'title' => __('Header Options', 'wpzoom'),
                'priority' => 50,
                'options' => array(
                    'top-navbar' => array(
                        'setting' => array(
                            'sanitize_callback' => 'sanitize_text_field',
                            'default' => 'block'
                        ),
                        'control' => array(
                            'label' => __('Show Top Navigation Menu', 'wpzoom'),
                            'type' => 'checkbox',
                        ),
                        'style' => array(
                            'selector' => '.top-navbar',
                            'rule' => 'display'
                        )
                    ),
                    'navbar-hide-search' => array(
                        'setting' => array(
                            'sanitize_callback' => 'sanitize_text_field',
                            'default' => 'block'
                        ),
                        'control' => array(
                            'label' => __('Show Search Form', 'wpzoom'),
                            'type' => 'checkbox',
                        ),
                        'style' => array(
                            'selector' => '.sb-search',
                            'rule' => 'display'
                        )
                    ),
                )
            ),
            'color' => array(
                'title' => __('General', 'wpzoom'),
                'panel' => 'color-scheme',
                'priority' => 110,
                'capability' => 'edit_theme_options',
                'options' => array(
                    'color-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#ffffff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Background Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'body',
                            'rule' => 'background'
                        )
                    ),
                    'color-body-text' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#444444'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Body Text', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'body',
                            'rule' => 'color'
                        )
                    ),
                    'color-headings' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#000'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Headings', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'h1, h2, h3, h4, h5, h6',
                            'rule' => 'color'
                        )
                    ),
                    'color-logo' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Logo Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.navbar-brand-wpz a',
                            'rule' => 'color'
                        ),
                    ),
                    'color-logo-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Logo Color on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.navbar-brand-wpz a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-tagline' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#afafaf'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Site Description', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.navbar-brand-wpz .tagline',
                            'rule' => 'color'
                        ),
                    ),
                    'color-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Link Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'a',
                            'rule' => 'color'
                        )
                    ),
                    'color-link-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Link Color on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'a:hover',
                            'rule' => 'color'
                        ),
                    ),
                    'color-button-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#231F20'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Buttons Background', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'button, input[type=button], input[type=reset], input[type=submit]',
                            'rule' => 'background'
                        ),
                    ),
                    'color-button-color' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Buttons Text Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'button, input[type=button], input[type=reset], input[type=submit]',
                            'rule' => 'color'
                        ),
                    ),
                    'color-button-background-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Buttons Background on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'button:hover, input[type=button]:hover, input[type=reset]:hover, input[type=submit]:hover',
                            'rule' => 'background'
                        ),
                    ),
                    'color-button-color-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Buttons Text Color on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => 'button:hover, input[type=button]:hover, input[type=reset]:hover, input[type=submit]:hover',
                            'rule' => 'color'
                        ),
                    ),
                ),

            ),
            'color-top-menu' => array(
                'panel' => 'color-scheme',
                'title' => __('Top Menu', 'wpzoom'),
                'options' => array(
                    'color-top-menu-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#121516'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Top Menu Background', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.top-navbar',
                            'rule' => 'background'
                        )
                    ),
                    'color-top-menu-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Menu Item', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.top-navbar .navbar-wpz > li > a',
                            'rule' => 'color'
                        )
                    ),
                    'color-top-menu-link-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Menu Item Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.top-navbar navbar-wpz > li > a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-top-menu-link-current' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Menu Current Item', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.top-navbar .navbar-wpz .current-menu-item > a, .top-navbar .navbar-wpz .current_page_item > a, .top-navbar .navbar-wpz .current-menu-parent > a',
                            'rule' => 'color'
                        )
                    ),
                    'color-search-icon-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#363940'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Search Icon Background', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.sb-search .sb-icon-search',
                            'rule' => 'background'
                        )
                    ),
                    'color-search-icon' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Search Icon Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.sb-search .sb-icon-search',
                            'rule' => 'color'
                        )
                    ),
                    'color-search-icon-background-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Search Icon Background on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.sb-search .sb-icon-search:hover, .sb-search .sb-search-input',
                            'rule' => 'background'
                        )
                    ),
                    'color-search-icon-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#ffffff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Search Icon Color on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.sb-search .sb-icon-search:hover, .sb-search .sb-search-input, .sb-search.sb-search-open .sb-icon-search:before',
                            'rule' => 'color'
                        )
                    )
                )
            ),
            'color-main-menu' => array(
                'panel' => 'color-scheme',
                'title' => __('Main Menu', 'wpzoom'),
                'options' => array(
                    'color-menu-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => ''
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Main Menu Background', 'wpzoom'),
                        ),
                        'style' => array(
                            array(
                                'selector' => '.main-navbar',
                                'rule' => 'background'
                            )
                        )
                    ),
                    'color-menu-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Menu Item', 'wpzoom'),
                        ),
                        'style' => array(
                            'id' => 'color-menu-link',
                            'selector' => '.main-navbar .navbar-wpz > li > a',
                            'rule' => 'color'
                        )
                    ),
                    'color-menu-link-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Menu Item Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.main-navbar .navbar-wpz > li > a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-menu-link-current' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Menu Current Item', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.main-navbar .navbar-wpz > .current-menu-item > a, .main-navbar .navbar-wpz > .current_page_item > a, .main-navbar .navbar-wpz > .current-menu-parent > a',
                            'rule' => 'color'
                        )
                    )
                )
            ),
            'color-slider' => array(
                'panel' => 'color-scheme',
                'title' => __('Homepage Slider', 'wpzoom'),
                'options' => array(
                    'color-slider-post-title' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Slide Title', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slide-indigo .slides li h3 a',
                            'rule' => 'color'
                        )
                    ),
                    'color-slider-post-title-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Slide Title Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slide-indigo .slides li h3 a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-slider-excerpt' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Slide Excerpt (on Pages)', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slides li .slide-header p',
                            'rule' => 'color'
                        )
                    ),

                    'color-slider-button-color' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Button Text', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slides li .slide_button a',
                            'rule' => 'color'
                        )
                    ),
                    'color-slider-button-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#000'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Button Background', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slides li .slide_button a',
                            'rule' => 'background'
                        )
                    ),
                    'color-slider-button-color-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Button Text Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slides li .slide_button a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-slider-button-background-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#000'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Button Background Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.slides li .slide_button a:hover',
                            'rule' => 'background'
                        )
                    ),

                )
            ),
            'color-posts' => array(
                'panel' => 'color-scheme',
                'title' => __('Blog Posts', 'wpzoom'),
                'options' => array(
                    'color-post-title' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Title', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.entry-title a',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-title-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Title Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.entry-title a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-cat' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Category', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.cat-links a',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-cat-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Category Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.cat-links a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-meta' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#676c71'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Meta', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.entry-meta',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-meta-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Meta Link', 'wpzoom'),
                        ),
                        'style' => array(
                            array(
                                'selector' => '.entry-meta a',
                                'rule' => 'color'
                            ),
                            array(
                                'selector' => '.recent-posts .entry-meta a',
                                'rule' => 'border-color'
                            )
                        )
                    ),
                    'color-post-meta-link-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Meta Link Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            array(
                                'selector' => '.entry-meta a:hover',
                                'rule' => 'color'
                            ),
                            array(
                                'selector' => '.recent-posts .entry-meta a:hover',
                                'rule' => 'border-color'
                            )
                        )
                    ),
                    'color-post-button-color' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Read More Text Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.readmore_button a',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-button-color-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Read More Text Color Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.readmore_button a:hover, .readmore_button a:active',
                            'rule' => 'color'
                        )
                    ),
                    'color-post-button-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#000'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Read More Button Background Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.readmore_button a',
                            'rule' => 'background-color'
                        )
                    ),
                    'color-post-button-background-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#000'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Read More Button Background Color Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.readmore_button a:hover, .readmore_button a:active',
                            'rule' => 'background-color'
                        )
                    ),
                )
            ),
            'color-navigation' => array(
                'panel' => 'color-scheme',
                'title' => __('Page Navigation', 'wpzoom'),
                'options' => array(
                    'color-infinite-button' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#000'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Jetpack Infinite Scroll Button', 'wpzoom'),
                            'description' => __('If you have the Infinite Scroll feature enabled, you can change here the color of the "Older Posts" button. You can find more instructions in <a href="http://www.wpzoom.com/documentation/tempo/#infinite" target="_blank">Documentation</a>', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.infinite-scroll #infinite-handle span',
                            'rule' => 'background'
                        )
                    ),

                    'color-infinite-button-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Jetpack Infinite Scroll Button Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.infinite-scroll #infinite-handle span:hover',
                            'rule' => 'background'
                        )
                    ),
                )
            ),
            'color-single' => array(
                'panel' => 'color-scheme',
                'title' => __('Individual Posts and Pages', 'wpzoom'),
                'options' => array(
                    'color-single-title' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post/Page Title', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.page h1.entry-title, .single h1.entry-title',
                            'rule' => 'color'
                        )
                    ),
                    'color-single-meta' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#676c71'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Meta', 'wpzoom'),
                        ),
                        'style' => array(
                            'id' => 'color-single-meta',
                            'selector' => '.single .entry-meta',
                            'rule' => 'color'
                        )
                    ),
                    'color-single-meta-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#222'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Meta Link', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.single .entry-meta a',
                            'rule' => 'color'
                        )
                    ),
                    'color-single-meta-link-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post Meta Link Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.single .entry-meta a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-single-content' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#444444'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Post/Page Text Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.entry-content',
                            'rule' => 'color'
                        )
                    ),
                    'color-single-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Links Color in Posts', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.entry-content a',
                            'rule' => 'color'
                        )
                    ),

                )
            ),
            'color-widgets' => array(
                'panel' => 'color-scheme',
                'title' => __('Widgets', 'wpzoom'),
                'options' => array(
                    'color-widget-title' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Widget Title Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.widget .title',
                            'rule' => 'color'
                        )
                    ),
                    'color-widget-title-background' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#121516'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Widget Title Background', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.widget .title',
                            'rule' => 'background'
                        )
                    ),

                )
            ),
            'color-footer' => array(
                'panel' => 'color-scheme',
                'title' => __('Footer', 'wpzoom'),
                'options' => array(
                    'footer-background-color' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#121516'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Background', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.site-footer',
                            'rule' => 'background-color'
                        )
                    ),
                    'footer-text-color' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#87888a'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Text Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.site-footer',
                            'rule' => 'color'
                        )
                    ),
                    'footer-widget-title' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Widget Title Color', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.site-footer .widget .title',
                            'rule' => 'color'
                        )
                    ),
                    'color-footer-link' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Links', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.site-footer a',
                            'rule' => 'color'
                        )
                    ),
                    'color-footer-link-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Links on Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.site-footer a:hover',
                            'rule' => 'color'
                        )
                    ),
                    'color-footer-menu' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#fff'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Menu Link', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.footer-menu ul li a',
                            'rule' => 'color'
                        )
                    ),

                    'color-footer-menu-hover' => array(
                        'setting' => array(
                            'sanitize_callback' => 'maybe_hash_hex_color',
                            'transport' => 'postMessage',
                            'default' => '#f45900'
                        ),
                        'control' => array(
                            'control_type' => 'WP_Customize_Color_Control',
                            'label' => __('Footer Menu Link Hover', 'wpzoom'),
                        ),
                        'style' => array(
                            'selector' => '.footer-menu ul li a:hover',
                            'rule' => 'color'
                        )
                    ),

                )
            ),
            /**
             *  Typography
             */
            'font-site-body' => array(
                'panel' => 'typography',
                'title' => __('Body', 'wpzoom'),
                'options' => array(
                    'body' => array(
                        'type' => 'typography',
                        'selector' => 'body',
                        'rules' => array(
                            'font-family' => 'Muli',
                            'font-size' => 16,
                            'font-weight' => 'normal',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-site-title' => array(
                'panel' => 'typography',
                'title' => __('Site Title', 'wpzoom'),
                'options' => array(
                    'title' => array(
                        'type' => 'typography',
                        'selector' => '.navbar-brand-wpz h1',
                        'rules' => array(
                            'font-family' => 'Oswald',
                            'font-size' => 70,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'description-typography' => array(
                'panel' => 'typography',
                'title' => __('Site Description', 'wpzoom'),
                'options' => array(
                    'description' => array(
                        'type' => 'typography',
                        'selector' => '.navbar-brand-wpz .tagline',
                        'rules' => array(
                            'font-family' => 'Muli',
                            'font-size' => 18,
                            'font-weight' => 'normal',
                            'text-transform' => 'none',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'topmenu-typography' => array(
                'panel' => 'typography',
                'title' => __('Top Menu Links', 'wpzoom'),
                'options' => array(
                    'topmenu' => array(
                        'type' => 'typography',
                        'selector' => '.top-navbar a',
                        'rules' => array(
                            'font-family' => 'Montserrat',
                            'font-size' => 12,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-nav' => array(
                'panel' => 'typography',
                'title' => __('Main Menu Links', 'wpzoom'),
                'options' => array(
                    'mainmenu' => array(
                        'type' => 'typography',
                        'selector' => '.main-navbar a',
                        'rules' => array(
                            'font-family' => 'Teko',
                            'font-size' => 28,
                            'font-weight' => 'normal',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-slider' => array(
                'panel' => 'typography',
                'title' => __('Homepage Slider Title', 'wpzoom'),
                'options' => array(
                    'slider-title' => array(
                        'type' => 'typography',
                        'selector' => '.slides li h3',
                        'rules' => array(
                            'font-family' => 'Teko',
                            'font-size' => 42,
                            'font-weight' => 'normal',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-slider-description' => array(
                'panel' => 'typography',
                'title' => __('Homepage Slider Description', 'wpzoom'),
                'options' => array(
                    'slider-text' => array(
                        'type' => 'typography',
                        'selector' => '.slides li .slide-header p',
                        'rules' => array(
                            'font-family' => 'Muli',
                            'font-size' => 18,
                            'font-weight' => 'normal',
                            'text-transform' => 'none',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-slider-button' => array(
                'panel' => 'typography',
                'title' => __('Homepage Slider Button', 'wpzoom'),
                'options' => array(
                    'slider-button' => array(
                        'type' => 'typography',
                        'selector' => '.slides li .slide_button a',
                        'rules' => array(
                            'font-family' => 'Montserrat',
                            'font-size' => 14,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-widgets' => array(
                'panel' => 'typography',
                'title' => __('Widget Title', 'wpzoom'),
                'options' => array(
                    'widget-title' => array(
                        'type' => 'typography',
                        'selector' => '.widget h3.title',
                        'rules' => array(
                            'font-family' => 'Montserrat',
                            'font-size' => 14,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-footer-widgets' => array(
                'panel' => 'typography',
                'title' => __('Footer Widget Title', 'wpzoom'),
                'options' => array(
                    'footer-widget-title' => array(
                        'type' => 'typography',
                        'selector' => '.site-footer .widget h3.title',
                        'rules' => array(
                            'font-family' => 'Montserrat',
                            'font-size' => 20,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-post-title' => array(
                'panel' => 'typography',
                'title' => __('Blog Posts Title', 'wpzoom'),
                'options' => array(
                    'blog-title' => array(
                        'type' => 'typography',
                        'selector' => '.entry-title',
                        'rules' => array(
                            'font-family' => 'Teko',
                            'font-size' => 36,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-single-post-title' => array(
                'panel' => 'typography',
                'title' => __('Single Post Title', 'wpzoom'),
                'options' => array(
                    'post-title' => array(
                        'type' => 'typography',
                        'selector' => '.single h1.entry-title',
                        'rules' => array(
                            'font-family' => 'Teko',
                            'font-size' => 40,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-page-title' => array(
                'panel' => 'typography',
                'title' => __('Single Page Title', 'wpzoom'),
                'options' => array(
                    'page-title' => array(
                        'type' => 'typography',
                        'selector' => '.page h1.entry-title',
                        'rules' => array(
                            'font-family' => 'Teko',
                            'font-size' => 40,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-footer-title' => array(
                'panel' => 'typography',
                'title' => __('Footer Site Title', 'wpzoom'),
                'options' => array(
                    'footer-title' => array(
                        'type' => 'typography',
                        'selector' => '.footer-title',
                        'rules' => array(
                            'font-family' => 'Oswald',
                            'font-size' => 40,
                            'font-weight' => 'bold',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'font-footer-menu' => array(
                'panel' => 'typography',
                'title' => __('Footer Menu', 'wpzoom'),
                'options' => array(
                    'footer-menu' => array(
                        'type' => 'typography',
                        'selector' => '.footer-menu ul li',
                        'rules' => array(
                            'font-family' => 'Teko',
                            'font-size' => 20,
                            'font-weight' => 'normal',
                            'text-transform' => 'uppercase',
                            'font-style' => 'normal'
                        )
                    )
                )
            ),
            'footer-area' => array(
                'title' => __('Footer', 'wpzoom'),
                'options' => array(

                    'footer-title' => array(
                        'setting' => array(
                            'sanitize_callback' => 'sanitize_text_field',
                            'default' => 'block'
                        ),
                        'control' => array(
                            'label' => __('Show Site Title in Footer', 'wpzoom'),
                            'type' => 'checkbox',
                        ),
                        'style' => array(
                            'selector' => '.footer-title',
                            'rule' => 'display'
                        )
                    ),

                    'blogcopyright' => array(
                        'setting' => array(
                            'sanitize_callback' => 'sanitize_text_field',
                            'default' => get_option('blogcopyright', sprintf( __( 'Copyright &copy; %1$s %2$s', 'wpzoom' ), date( 'Y' ), get_bloginfo( 'name' ) )),
                            'transport' => 'postMessage',
                            'type' => 'option'
                        ),
                        'control' => array(
                            'label' => __('Footer Text', 'wpzoom'),
                            'type' => 'text',
                            'priority' => 10
                        ),
                        'partial' => array(
                            'selector' => '.site-info .copyright',
                            'container_inclusive' => false,
                            'render_callback' => 'zoom_customizer_partial_blogcopyright'
                        )

                    )
                )
            )
        );

        zoom_customizer_normalize_options($data);
    }


    return $data;
}

add_filter('wpzoom_customizer_data', 'indigo_customizer_data');