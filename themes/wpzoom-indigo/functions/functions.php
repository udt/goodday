<?php
/**
 * Theme functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 */

if ( ! function_exists( 'indigo_setup' ) ) :
/**
 * Theme setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 */
function indigo_setup() {
    // This theme styles the visual editor to resemble the theme style.
    add_editor_style( array( 'css/editor-style.css' ) );

    /* Image Sizes */
    add_image_size( 'loop', 380, 380, true );               /* Default list view */
    add_image_size( 'loop-retina', 760, 760, true );        /* Default list view@2x */
    add_image_size( 'loop-cols', 380, 250, true );          /* For 2 columns with sidebar and 3 columns (always) */
    add_image_size( 'loop-cols', 760, 500, true );          /* For 2 columns with sidebar and 3 columns (always) @ 2x */
    add_image_size( 'loop-cols-large', 576, 384, true );    /* For 2 columns in full-width */
    add_image_size( 'loop-cols-large-retina', 1152, 768, true );    /* For 2 columns in full-width @ 2x */
    add_image_size( 'loop-large', 800 );                    /* For Blog view/ Individual posts */
    add_image_size( 'loop-large-cropped', 800, 500, true ); /* For Individual posts/pages, cropped */
    add_image_size( 'loop-full', 1200, 650, true );         /* For Full-width Posts at the Top */
    add_image_size( 'loop-full-retina', 2400, 1300, true ); /* For Full-width Posts at the Top */

    add_image_size( 'widget-small', 165, 109, true);        /* Featured Category Widget */

    add_image_size( 'image-box-widget', 372, 230, true);    /* Image Box Widget */
    add_image_size( 'image-box-widget-retina', 744, 460, true); /* Image Box Widget @ 2x*/



    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    // Register nav menus
    register_nav_menus( array(
        'secondary' => __( 'Top Menu', 'wpzoom' ),
        'primary' => __( 'Main Menu', 'wpzoom' ),
        'mobile' => __( 'Mobile Menu', 'wpzoom' ),
        'tertiary' => __( 'Footer Menu', 'wpzoom' )

    ) );


    /*
     * JetPack Infinite Scroll
     */
    add_theme_support( 'infinite-scroll', array(
        'container' => 'recent-posts',
        'wrapper' => false,
        'footer' => false
    ) );

    /**
     * Theme Logo
     */
    add_theme_support( 'custom-logo', array(
        'height'      => 150,
        'width'       => 650,
        'flex-height' => true,
        'flex-width'  => true
    ) );


}
endif;
add_action( 'after_setup_theme', 'indigo_setup' );



/*  Add support for Custom Background
==================================== */

add_theme_support( 'custom-background' );


/*  Add Support for Shortcodes in Excerpt
========================================== */

add_filter( 'the_excerpt', 'shortcode_unautop' );
add_filter( 'the_excerpt', 'do_shortcode' );

add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );



/*  Recommended Plugins
========================================== */
function zoom_register_theme_required_plugins_callback($plugins){

    $plugins =  array_merge(array(

        array(
            'name'         => 'Jetpack',
            'slug'         => 'jetpack',
            'required'     => false,
        ),

        array(
            'name'         => 'Instagram Widget by WPZOOM',
            'slug'         => 'instagram-widget-by-wpzoom',
            'required'     => false,
        ),

        array(
            'name'         => 'Social Warfare',
            'slug'         => 'social-warfare',
            'required'     => false,
        )


    ), $plugins);

    return $plugins;
}

add_filter('zoom_register_theme_required_plugins', 'zoom_register_theme_required_plugins_callback');




/*  Let users change "Older Posts" button text from Jetpack Infinite Scroll
========================================== */

function indigo_infinite_scroll_js_settings( $settings ) {
    $settings['text'] = esc_js( esc_html( option::get( 'infinite_scroll_handle_text' ) ) );

    return $settings;
}
add_filter( 'infinite_scroll_js_settings', 'indigo_infinite_scroll_js_settings' );


/* Enable Excerpts for Pages
==================================== */

add_action( 'init', 'wpzoom_excerpts_to_pages' );
function wpzoom_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}



/* Disable Jetpack Related Posts on Post Type
========================================== */

function indigo_no_related_posts( $options ) {
    if ( !is_singular( 'post' ) ) {
        $options['enabled'] = false;
    }
    return $options;
}
add_filter( 'jetpack_relatedposts_filter_options', 'indigo_no_related_posts' );




/*  Custom Excerpt Length
==================================== */

function new_excerpt_length( $length ) {
    return (int) option::get( "excerpt_length" ) ? (int)option::get( "excerpt_length" ) : 50;
}

add_filter( 'excerpt_length', 'new_excerpt_length' );



/*  Maximum width for images in posts
=========================================== */

if ( ! isset( $content_width ) ) $content_width = 800;

function indigo_content_width() {
   if ( is_page_template( 'page-templates/full-width.php' ) ) {
            global $content_width;
           $content_width = 1200;
   }
}

add_action( 'template_redirect', 'indigo_content_width' );



if ( ! function_exists( 'indigo_get_the_archive_title' ) ) :
/* Custom Archives titles.
=================================== */
function indigo_get_the_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }

    return $title;
}
endif;
add_filter( 'get_the_archive_title', 'indigo_get_the_archive_title' );



if ( ! function_exists( 'indigo_alter_main_query' ) ) :
/**
 * Alter main WordPress Query to exclude specific categories
 * and posts from featured slider if this is configured via Theme Options.
 *
 * @param $query WP_Query
 */
function indigo_alter_main_query( $query ) {
    // until this is fixed https://core.trac.wordpress.org/ticket/27015
    $is_front = false;

    if ( get_option( 'page_on_front' ) == 0 ) {
        $is_front = is_front_page();
    } else {
        $is_front = $query->get( 'page_id' ) == get_option( 'page_on_front' );
    }

    if ( $query->is_main_query() && $is_front ) {
        if ( option::is_on( 'hide_featured' ) ) {
            $featured_posts = new WP_Query( array(
                'post__not_in'   => get_option( 'sticky_posts' ),
                'posts_per_page' => option::get( 'slideshow_posts' ),
                'meta_key'       => 'wpzoom_is_featured',
                'meta_value'     => 1
            ) );

            $postIDs = array();
            while ( $featured_posts->have_posts() ) {
                $featured_posts->the_post();
                $postIDs[] = get_the_ID();
            }

            wp_reset_postdata();

            $query->set( 'post__not_in', $postIDs );
        }

        if ( count( option::get( 'recent_part_exclude' ) ) ) {
            $query->set( 'cat', '-' . implode( ',-', (array) option::get('recent_part_exclude') ) );
        }
    }
}
endif;
add_action( 'pre_get_posts', 'indigo_alter_main_query' );




/* Register Custom Fields in Profile: Facebook, Twitter
===================================================== */

add_action( 'show_user_profile', 'indigo_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'indigo_show_extra_profile_fields' );

function indigo_show_extra_profile_fields( $user ) { ?>

    <h3><?php _e('Additional Profile Information', 'wpzoom'); ?></h3>

    <table class="form-table">


        <tr>
            <th><label for="twitter"><?php _e('Twitter Username', 'wpzoom'); ?></label></th>

            <td>
                <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><?php _e('Please enter your Twitter username', 'wpzoom'); ?></span>
            </td>
        </tr>

        <tr>
            <th><label for="facebook_url"><?php _e('Facebook Profile URL', 'wpzoom'); ?></label></th>

            <td>
                <input type="text" name="facebook_url" id="facebook_url" value="<?php echo esc_attr( get_the_author_meta( 'facebook_url', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><?php _e('Please enter your Facebook profile URL', 'wpzoom'); ?></span>
            </td>
        </tr>

        <tr>
            <th><label for="facebook_url"><?php _e('Instagram Username', 'wpzoom'); ?></label></th>

            <td>
                <input type="text" name="instagram_url" id="instagram_url" value="<?php echo esc_attr( get_the_author_meta( 'instagram_url', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><?php _e('Please enter your Instagram username', 'wpzoom'); ?></span>
            </td>
        </tr>

    </table>
<?php }

add_action( 'personal_options_update', 'indigo_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'indigo_save_extra_profile_fields' );

function indigo_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    update_user_meta( $user_id, 'instagram_url', $_POST['instagram_url'] );
    update_user_meta( $user_id, 'facebook_url', $_POST['facebook_url'] );
    update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
}





/* Count post views
==================================== */

add_action( 'template_redirect', 'entry_views_load' );
add_action( 'wp_ajax_entry_views', 'entry_views_update_ajax' );
add_action( 'wp_ajax_nopriv_entry_views', 'entry_views_update_ajax' );

function entry_views_load() {
  global $wp_query, $entry_views_pid;

  if ( is_singular() ) {

    $post = $wp_query->get_queried_object();
        $entry_views_pid = $post->ID;
        wp_enqueue_script( 'jquery' );
        add_action( 'wp_footer', 'entry_views_load_scripts' );
  }
}

function entry_views_update( $post_id = '' ) {
  global $wp_query;

  if ( !empty( $post_id ) ) {

    $meta_key = apply_filters( 'entry_views_meta_key', 'Views' );
    $old_views = get_post_meta( $post_id, $meta_key, true );
    $new_views = absint( $old_views ) + 1;
    update_post_meta( $post_id, $meta_key, $new_views, $old_views );
  }
}


function entry_views_get( $attr = '' ) {
  global $post;

  $attr = shortcode_atts( array( 'before' => '', 'after' => '', 'post_id' => $post->ID ), $attr );
  $meta_key = apply_filters( 'entry_views_meta_key', 'Views' );
  $views = intval( get_post_meta( $attr['post_id'], $meta_key, true ) );
  return $attr['before'] . number_format_i18n( $views ) . $attr['after'];
}


function entry_views_update_ajax() {

  check_ajax_referer( 'entry_views_ajax' );

  if ( isset( $_POST['post_id'] ) )
    $post_id = absint( $_POST['post_id'] );

  if ( !empty( $post_id ) )
    entry_views_update( $post_id );
}


function entry_views_load_scripts() {
  global $entry_views_pid;

  $nonce = wp_create_nonce( 'entry_views_ajax' );

  echo '<script type="text/javascript">/* <![CDATA[ */ jQuery(document).ready( function() { jQuery.post( "' . admin_url( 'admin-ajax.php' ) . '", { action : "entry_views", _ajax_nonce : "' . $nonce . '", post_id : ' . $entry_views_pid . ' } ); } ); /* ]]> */</script>' . "\n";
}




/**
 * Show custom logo or blog title and description (backward compatibility)
 *
 */
function indigo_custom_logo()
{
    //In future must remove it is for backward compatibility.
    if(get_theme_mod('logo')){
        set_theme_mod('custom_logo',  zoom_get_attachment_id_from_url(get_theme_mod('logo')));
        remove_theme_mod('logo');
    }

    has_custom_logo() ? the_zoom_custom_logo() : printf('<h1><a href="%s" title="%s">%s</a></h1>', home_url(), get_bloginfo('description'), get_bloginfo('name'));

}



if ( ! function_exists( 'indigo_get_google_font_uri' ) ) :
    /**
     * Build the HTTP request URL for Google Fonts.
     *
     * @return string    The URL for including Google Fonts.
     */
    function indigo_get_google_font_uri() {
        // Grab the font choices
        $font_keys = zoom_customizer_get_font_familiy_ids(indigo_customizer_data());

        $fonts = array();
        foreach ( $font_keys as $key => $default ) {
            $fonts[] = get_theme_mod( $key, $default );
        }

        // De-dupe the fonts
        $fonts         = array_unique( $fonts );
        $allowed_fonts = zoom_customizer_get_google_fonts();
        $family        = array();

        // Validate each font and convert to URL format
        foreach ( $fonts as $font ) {
            $font = trim( $font );

            // Verify that the font exists
            if ( array_key_exists( $font, $allowed_fonts ) ) {
                // Build the family name and variant string (e.g., "Open+Sans:regular,italic,700")
                $family[] = urlencode( $font . ':' . join( ',', zoom_customizer_choose_google_font_variants( $font, $allowed_fonts[ $font ]['variants'] ) ) );
            }
        }

        // Convert from array to string
        if ( empty( $family ) ) {
            return '';
        } else {
            $request = '//fonts.googleapis.com/css?family=' . implode( '|', $family );
        }

        // Load the font subset
        $subset = get_theme_mod( 'font-subset', false );

        if ( 'all' === $subset ) {

            $subsets_available = zoom_customizer_get_google_font_subsets();

            // Remove the all set
            unset( $subsets_available['all'] );

            // Build the array
            $subsets = array_keys( $subsets_available );
        } else {
            $subsets = array(
                'latin',
                $subset,
            );
        }

        // Append the subset string
        if ( ! empty( $subsets ) ) {
            $request .= urlencode( '&subset=' . join( ',', $subsets ) );
        }

        /**
         * Filter the Google Fonts URL.
         *
         * @since 1.2.3.
         *
         * @param string    $url    The URL to retrieve the Google Fonts.
         */
        return apply_filters( 'indigo_get_google_font_uri', esc_url( $request ) );
    }
endif;


/* Enqueue scripts and styles for the front end.
=========================================== */

function indigo_scripts() {
    if ( '' !== $google_request = indigo_get_google_font_uri() ) {
        wp_enqueue_style( 'indigo-google-fonts', $google_request, WPZOOM::$themeVersion );
    }

    // Load our main stylesheet.
    wp_enqueue_style( 'indigo-style', get_stylesheet_uri(), array(), WPZOOM::$themeVersion );

    wp_enqueue_style( 'media-queries', get_template_directory_uri() . '/css/media-queries.css', array(), WPZOOM::$themeVersion );

    wp_enqueue_style( 'indigo-google-font-default', '//fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i|Muli:400,400i,600,600i,700,700i,800,800i|Oswald:400,500,700|Teko:400,700&amp;subset=cyrillic,latin-ext' );

    $color_scheme = get_theme_mod('color-palettes', zoom_customizer_get_default_option_value('color-palettes', indigo_customizer_data()));
    wp_enqueue_style('indigo-style-color-' . $color_scheme, get_template_directory_uri() . '/styles/' . $color_scheme . '.css', array(), WPZOOM::$themeVersion);

    wp_enqueue_style( 'dashicons' );

    wp_enqueue_script( 'slicknav', get_template_directory_uri() . '/js/jquery.slicknav.min.js', array( 'jquery' ), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array(), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.min.js', array( 'jquery' ), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'retina', get_template_directory_uri() . '/js/retina.min.js', array('underscore'), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'search_button', get_template_directory_uri() . '/js/search_button.js', array(), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'tabber-tabs', get_template_directory_uri() . '/js/tabs.js', array( 'jquery' ), WPZOOM::$themeVersion, true );

    wp_enqueue_script( 'sticky-sidebar', get_template_directory_uri() . '/js/theia-sticky-sidebar.js', array( 'jquery' ), WPZOOM::$themeVersion, true );


    $themeJsOptions = option::getJsOptions();

    wp_enqueue_script( 'indigo-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), WPZOOM::$themeVersion, true );
    wp_localize_script( 'indigo-script', 'zoomOptions', $themeJsOptions );
}

add_action( 'wp_enqueue_scripts', 'indigo_scripts' );