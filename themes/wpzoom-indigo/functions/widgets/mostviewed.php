<?php

/*------------------------------------------*/
/* WPZOOM: Popular Tabs Widget              */
/*------------------------------------------*/

class Wpzoom_Popular_Views extends WP_Widget {

	function __construct() {

		/* Widget settings. */
		$widget_ops = array( 'classname' => 'popular-views', 'description' => 'A list of the most viewed posts' );

		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'wpzoom-popular-views' );

		/* Create the widget. */
		parent::__construct( 'wpzoom-popular-views', 'WPZOOM: Most Viewed', $widget_ops, $control_ops );

	}

	function widget( $args, $instance ) {

		extract( $args );

		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$show_count = $instance['show_count'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		?>
			<?php $loop = new WP_Query( array( 'ignore_sticky_posts' => true, 'showposts' => $show_count,  'meta_key' => 'Views', 'orderby' => 'meta_value_num', 'order' => 'DESC' ) ); ?>

			<ol class="popular-posts">
				<?php if ( $loop->have_posts() ) : ?>
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<li><div class="list_wrapper"><h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3> <span><?php printf( get_post_meta( get_the_ID(), 'Views', true ) );  ?> <?php _e('views', 'wpzoom'); ?></span></div></li>
				<?php endwhile; ?>
			</ol>

			<?php endif; ?>
		 <?php

		/* After widget (defined by themes). */
		echo $after_widget;

	}

	function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_count'] = $new_instance['show_count'];

		return $instance;

	}

	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Most Viewed', 'show_count' => 5 );
		$instance = wp_parse_args( (array) $instance, $defaults );


		?>

			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'wpzoom'); ?></label><br />
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" type="text"  class="widefat" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'show_count' ); ?>">Show:</label>
				<input id="<?php echo $this->get_field_id( 'show_count' ); ?>" name="<?php echo $this->get_field_name( 'show_count' ); ?>" value="<?php echo $instance['show_count']; ?>" type="text" size="2" /> posts
			</p>

		<?php

	}

}

add_action('widgets_init', create_function('','register_widget("Wpzoom_Popular_Views");'));