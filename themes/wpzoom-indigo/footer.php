<?php
/**
 * The template for displaying the footer
 *
 */

?>

    </div><!-- ./inner-wrap -->

    <footer id="colophon" class="site-footer" role="contentinfo">
        <div class="footer-widgets widgets">
            <div class="inner-wrap">
                <div class="widget-areas">
                    <?php if ( is_active_sidebar( 'footer_1' ) ) : ?>
                        <div class="column">
                            <?php dynamic_sidebar('footer_1'); ?>
                            <div class="clear"></div>
                        </div><!-- end .column -->
                    <?php endif; ?>

                    <?php if ( is_active_sidebar( 'footer_2' ) ) : ?>
                        <div class="column">
                            <?php dynamic_sidebar('footer_2'); ?>
                            <div class="clear"></div>
                        </div><!-- end .column -->
                    <?php endif; ?>

                    <?php if ( is_active_sidebar( 'footer_3' ) ) : ?>
                        <div class="column">
                            <?php dynamic_sidebar('footer_3'); ?>
                            <div class="clear"></div>
                        </div><!-- end .column -->
                    <?php endif; ?>

                    <?php if ( is_active_sidebar( 'footer_4' ) ) : ?>
                        <div class="column">
                            <?php dynamic_sidebar('footer_4'); ?>
                            <div class="clear"></div>
                        </div><!-- end .column -->
                    <?php endif; ?>
                </div><!-- .widget-areas -->
            </div><!-- .inner-wrap -->
        </div><!-- .footer-widgets -->


        <?php if ( is_active_sidebar( 'widgetized_section' ) ) : ?>
            <section class="site-widgetized-section section-footer">
                <div class="widgets clearfix">
                    <?php dynamic_sidebar( 'widgetized_section' ); ?>
                </div>
            </section><!-- .site-widgetized-section -->
        <?php endif; ?>

        <div class="site-info">

            <div class="inner-wrap">
                <h2 class="footer-title"><a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'description' ); ?>">
                    <?php bloginfo( 'name' ); ?>
                </a></h2>
                <?php if ( has_nav_menu( 'tertiary' ) ) { ?>
                    <div class="footer-menu">
                        <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'container_class' => 'menu-footer', 'theme_location' => 'tertiary', 'depth' => '1' ) ); ?>
                    </div>
                <?php } ?>
            </div>

            <div class="site-copyright">
                <div class="inner-wrap">
                    <span class="copyright"><?php zoom_customizer_partial_blogcopyright(); ?></span> <span class="designed-by">&mdash; <?php printf( __( 'Designed by %s', 'wpzoom' ), '<a href="http://www.wpzoom.com/" target="_blank" rel="designer">WPZOOM</a>' ); ?></span>
                </div>
            </div><!-- .site-copyright -->

        </div><!-- .site-info -->

    </footer><!-- #colophon -->

</div><!-- /.page-wrap -->

<?php wp_footer(); ?>

</body>
</html>