<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="page-wrap">

    <header class="site-header">

        <nav class="top-navbar" role="navigation">

            <div class="inner-wrap">

                <div id="navbar-top">
                    <?php if (has_nav_menu( 'secondary' )) {
                        wp_nav_menu( array(
                            'menu_class'     => 'navbar-wpz dropdown sf-menu',
                            'theme_location' => 'secondary'
                        ) );
                    } ?>
               </div><!-- #navbar-top -->

                <div id="sb-search" class="sb-search">
                    <?php get_search_form(); ?>
                </div><!-- .sb-search -->

                <div class="header_social">
                    <?php dynamic_sidebar('header_social'); ?>
                </div><!-- .header_social -->

            </div><!-- ./inner-wrap -->

        </nav><!-- .navbar -->
        <div class="clear"></div>

        <div class="inner-wrap">

            <div class="navbar-brand-wpz<?php if (option::is_on('ad_head_select')) { ?> left-align<?php } ?>">

                <?php indigo_custom_logo() ?>

                <p class="tagline"><?php bloginfo('description')  ?></p>

            </div><!-- .navbar-brand -->


            <?php if (option::is_on('ad_head_select')) { ?>
                <div class="adv">

                    <?php if ( option::get('ad_head_code') <> "") {
                        echo stripslashes(option::get('ad_head_code'));
                    } else { ?>
                        <a href="<?php echo option::get('banner_top_url'); ?>"><img src="<?php echo option::get('banner_top'); ?>" alt="<?php echo option::get('banner_top_alt'); ?>" /></a>
                    <?php } ?>

                </div><!-- /.adv --> <div class="clear"></div>
            <?php } ?>


            <nav class="main-navbar" role="navigation">

                <div class="navbar-header-main">
                    <?php if ( has_nav_menu( 'mobile' ) ) { ?>

                       <?php wp_nav_menu( array(
                           'container_id'   => 'menu-main-slide',
                           'theme_location' => 'mobile',
                           'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s' . indigo_wc_menu_cartitem() . '</ul>'
                       ) );
                   } elseif ( has_nav_menu( 'primary' ) ) {
                        wp_nav_menu( array(
                           'container_id'   => 'menu-main-slide',
                          'theme_location' => 'primary',
                          'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s' . indigo_wc_menu_cartitem() . '</ul>'
                       ) );
                    } ?>

                </div>

                <div id="navbar-main">

                    <?php if (has_nav_menu( 'primary' )) {
                        wp_nav_menu( array(
                            'menu_class'     => 'navbar-wpz dropdown sf-menu',
                            'theme_location' => 'primary',
                            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s' . indigo_wc_menu_cartitem() . '</ul>'
                        ) );
                    } ?>

                </div><!-- #navbar-main -->

            </nav><!-- .navbar -->
            <div class="clear"></div>

        </div><!-- .inner-wrap -->

    </header><!-- .site-header -->

    <div class="inner-wrap">