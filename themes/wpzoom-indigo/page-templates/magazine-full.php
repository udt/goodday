<?php
/**
 Template Name: Homepage: Magazine (Full-width)
 */

get_header(); ?>

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // gets current page number

$template = option::get( 'layout_home' ); ?>


<?php if ( option::is_on( 'featured_posts_show' )  && $paged < 2) : ?>

    <?php get_template_part( 'wpzoom-slider' ); ?>

<?php endif; ?>

<?php if ( is_active_sidebar( 'homepage-top' ) && $paged < 2  ) : ?>

    <section class="home-widgetized-sections">

        <?php dynamic_sidebar( 'homepage-top' ); ?>

    </section><!-- .home-widgetized-sections -->

<?php endif; ?>



<main id="main" class="site-main" role="main">

    <section class="content-area full-layout">

            <?php if ( is_active_sidebar( 'home-categories' ) ) { // Widget area for Featured Categories ?>

                <?php dynamic_sidebar('home-categories'); ?>

            <?php } ?>


        <div class="clear"></div>

    </section><!-- .content-area -->


</main><!-- .site-main -->

<?php
get_footer();
