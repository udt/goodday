<?php
/**
 Template Name: Blog
 */

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // gets current page number

get_header(); ?>

<?php $template = option::get( 'layout_home' ); ?>

<main id="main" class="site-main" role="main">

    <section class="content-area<?php if ( 'full' == $template || option::get('post_view') == '3 Columns' ) { echo ' full-layout'; } ?>">

        <h2 class="section-title<?php if ( ( $template  == 'full' ) && (option::get('post_view') == 'Blog') ) { echo ' full-title'; } ?>"><?php the_title(); ?></h2>


        <?php if ( have_posts() ) : ?>

            <section id="recent-posts" class="recent-posts<?php if (option::get('post_view') == 'Blog') { echo " blog-view"; } elseif (option::get('post_view') == '2 Columns') { echo " two-columns_layout"; } elseif ( option::get('post_view') == '3 Columns') { echo " three-columns_layout"; } ?>">

                <?php
                if ( get_query_var('paged') )
                    $paged = get_query_var('paged');
                elseif ( get_query_var('page') )
                    $paged = get_query_var('page');
                else
                    $paged = 1;

                query_posts("post_type=post&paged=$paged");
                ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content', get_post_format() ); ?>

                <?php endwhile; ?>


            </section>

               <?php get_template_part( 'pagination' ); ?>

           <?php else: ?>

               <?php get_template_part( 'content', 'none' ); ?>

           <?php endif; ?>


        <div class="clear"></div>

    </section><!-- .content-area -->

    <?php if ( !( 'full' == $template  ||  option::get('post_view') == '3 Columns' ) ) : ?>

        <?php get_sidebar(); ?>

    <?php else : ?>

        <div class="clear"></div>

    <?php endif; ?>


</main><!-- .site-main -->

<?php
get_footer();
