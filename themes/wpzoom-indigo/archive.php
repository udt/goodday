<?php get_header(); ?>

<?php $template = option::get( 'layout_archive' ); ?>

<main id="main" class="site-main" role="main">

    <section class="content-area<?php if ( 'full' == $template || option::get('post_view_archive') == '3 Columns' ) { echo ' full-layout'; } ?>">

        <div class="header-archive">

            <?php the_archive_title( '<h2 class="section-title">', '</h2>' ); ?>

            <?php if (is_category() ) { ?><?php echo category_description(); ?><?php } ?>

        </div>

        <?php if ( have_posts() ) : ?>

            <section id="recent-posts" class="recent-posts<?php if (option::get('post_view_archive') == 'Blog') { echo " blog-view"; } elseif (option::get('post_view_archive') == '2 Columns') { echo " two-columns_layout"; } elseif ( option::get('post_view_archive') == '3 Columns') { echo " three-columns_layout"; } ?>">

                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'content-archive', get_post_format() ); ?>
                <?php endwhile; ?>

            </section><!-- .recent-posts -->

            <?php get_template_part( 'pagination' ); ?>

        <?php else: ?>

            <?php get_template_part( 'content', 'none' ); ?>

        <?php endif; ?>

    </section><!-- .content-area -->

    <?php if ( !( 'full' == $template  ||  option::get('post_view_archive') == '3 Columns' ) ) : ?>

        <?php get_sidebar(); ?>

    <?php else : ?>

        <div class="clear"></div>

    <?php endif; ?>

</main><!-- .site-main -->

<?php
get_footer();
